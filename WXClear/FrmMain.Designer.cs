﻿namespace WXClear
{
    partial class FrmMain
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new Sunny.UI.UILabel();
            this.m_WXFilesPathTextBox = new Sunny.UI.UITextBox();
            this.m_PathBrowerBtn = new Sunny.UI.UISymbolButton();
            this.m_TabControl = new Sunny.UI.UITabControl();
            this.m_TabPageDocuments = new System.Windows.Forms.TabPage();
            this.m_StatusStrip = new System.Windows.Forms.StatusStrip();
            this.m_LabelMessage = new System.Windows.Forms.ToolStripStatusLabel();
            this.m_ListViewDoc = new System.Windows.Forms.ListView();
            this.m_ColumnHeaderNumber = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.m_ColumnHeaderName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.m_ColumnHeaderSize = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.m_ColumnHeaderDate = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.m_ColumnHeaderFullPath = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.m_TabPageImages = new System.Windows.Forms.TabPage();
            this.m_ListViewImage = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.m_TabPageVideos = new System.Windows.Forms.TabPage();
            this.m_ListViewVideo = new System.Windows.Forms.ListView();
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader8 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader9 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader10 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.m_FindRepeatBtn = new Sunny.UI.UISymbolButton();
            this.m_DelBtn = new Sunny.UI.UISymbolButton();
            this.m_GroupBox = new Sunny.UI.UIGroupBox();
            this.m_UITextBoxVideos = new Sunny.UI.UITextBox();
            this.m_UITextBoxImages = new Sunny.UI.UITextBox();
            this.m_UITextBoxDocs = new Sunny.UI.UITextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.m_UICheckBoxVideos = new Sunny.UI.UICheckBox();
            this.m_UICheckBoxImages = new Sunny.UI.UICheckBox();
            this.m_UICheckBoxDocs = new Sunny.UI.UICheckBox();
            this.m_RadioButtonOld = new Sunny.UI.UICheckBox();
            this.m_TabControl.SuspendLayout();
            this.m_TabPageDocuments.SuspendLayout();
            this.m_StatusStrip.SuspendLayout();
            this.m_TabPageImages.SuspendLayout();
            this.m_TabPageVideos.SuspendLayout();
            this.m_GroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.label1.Location = new System.Drawing.Point(12, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(136, 21);
            this.label1.Style = Sunny.UI.UIStyle.LightBlue;
            this.label1.TabIndex = 0;
            this.label1.Text = "待 清 理 路 径  ：";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // m_WXFilesPathTextBox
            // 
            this.m_WXFilesPathTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.m_WXFilesPathTextBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.m_WXFilesPathTextBox.Enabled = false;
            this.m_WXFilesPathTextBox.FillColor = System.Drawing.Color.White;
            this.m_WXFilesPathTextBox.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.m_WXFilesPathTextBox.Location = new System.Drawing.Point(166, 23);
            this.m_WXFilesPathTextBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.m_WXFilesPathTextBox.Maximum = 2147483647D;
            this.m_WXFilesPathTextBox.Minimum = -2147483648D;
            this.m_WXFilesPathTextBox.MinimumSize = new System.Drawing.Size(1, 1);
            this.m_WXFilesPathTextBox.Name = "m_WXFilesPathTextBox";
            this.m_WXFilesPathTextBox.Padding = new System.Windows.Forms.Padding(5);
            this.m_WXFilesPathTextBox.Size = new System.Drawing.Size(366, 29);
            this.m_WXFilesPathTextBox.Style = Sunny.UI.UIStyle.LightBlue;
            this.m_WXFilesPathTextBox.TabIndex = 1;
            // 
            // m_PathBrowerBtn
            // 
            this.m_PathBrowerBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.m_PathBrowerBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.m_PathBrowerBtn.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(243)))), ((int)(((byte)(255)))));
            this.m_PathBrowerBtn.FillHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(160)))), ((int)(((byte)(255)))));
            this.m_PathBrowerBtn.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.m_PathBrowerBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(160)))), ((int)(((byte)(255)))));
            this.m_PathBrowerBtn.Location = new System.Drawing.Point(544, 21);
            this.m_PathBrowerBtn.MinimumSize = new System.Drawing.Size(1, 1);
            this.m_PathBrowerBtn.Name = "m_PathBrowerBtn";
            this.m_PathBrowerBtn.RectHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(160)))), ((int)(((byte)(255)))));
            this.m_PathBrowerBtn.Size = new System.Drawing.Size(237, 30);
            this.m_PathBrowerBtn.Style = Sunny.UI.UIStyle.LightBlue;
            this.m_PathBrowerBtn.Symbol = 57437;
            this.m_PathBrowerBtn.TabIndex = 2;
            this.m_PathBrowerBtn.Text = "浏览";
            // 
            // m_TabControl
            // 
            this.m_TabControl.Controls.Add(this.m_TabPageDocuments);
            this.m_TabControl.Controls.Add(this.m_TabPageImages);
            this.m_TabControl.Controls.Add(this.m_TabPageVideos);
            this.m_TabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_TabControl.DrawMode = System.Windows.Forms.TabDrawMode.OwnerDrawFixed;
            this.m_TabControl.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.m_TabControl.ItemSize = new System.Drawing.Size(150, 40);
            this.m_TabControl.Location = new System.Drawing.Point(0, 229);
            this.m_TabControl.MainPage = "";
            this.m_TabControl.Name = "m_TabControl";
            this.m_TabControl.SelectedIndex = 0;
            this.m_TabControl.Size = new System.Drawing.Size(801, 194);
            this.m_TabControl.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.m_TabControl.Style = Sunny.UI.UIStyle.LightBlue;
            this.m_TabControl.TabIndex = 3;
            // 
            // m_TabPageDocuments
            // 
            this.m_TabPageDocuments.Controls.Add(this.m_StatusStrip);
            this.m_TabPageDocuments.Controls.Add(this.m_ListViewDoc);
            this.m_TabPageDocuments.Location = new System.Drawing.Point(0, 40);
            this.m_TabPageDocuments.Name = "m_TabPageDocuments";
            this.m_TabPageDocuments.Size = new System.Drawing.Size(801, 154);
            this.m_TabPageDocuments.TabIndex = 0;
            this.m_TabPageDocuments.Text = "文档";
            this.m_TabPageDocuments.UseVisualStyleBackColor = true;
            // 
            // m_StatusStrip
            // 
            this.m_StatusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.m_LabelMessage});
            this.m_StatusStrip.Location = new System.Drawing.Point(0, 132);
            this.m_StatusStrip.Name = "m_StatusStrip";
            this.m_StatusStrip.Size = new System.Drawing.Size(801, 22);
            this.m_StatusStrip.TabIndex = 1;
            this.m_StatusStrip.Text = "statusStrip1";
            // 
            // m_LabelMessage
            // 
            this.m_LabelMessage.Name = "m_LabelMessage";
            this.m_LabelMessage.Size = new System.Drawing.Size(44, 17);
            this.m_LabelMessage.Text = "         ";
            // 
            // m_ListViewDoc
            // 
            this.m_ListViewDoc.CheckBoxes = true;
            this.m_ListViewDoc.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.m_ColumnHeaderNumber,
            this.m_ColumnHeaderName,
            this.m_ColumnHeaderSize,
            this.m_ColumnHeaderDate,
            this.m_ColumnHeaderFullPath});
            this.m_ListViewDoc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_ListViewDoc.FullRowSelect = true;
            this.m_ListViewDoc.GridLines = true;
            this.m_ListViewDoc.HideSelection = false;
            this.m_ListViewDoc.Location = new System.Drawing.Point(0, 0);
            this.m_ListViewDoc.Name = "m_ListViewDoc";
            this.m_ListViewDoc.Size = new System.Drawing.Size(801, 154);
            this.m_ListViewDoc.TabIndex = 0;
            this.m_ListViewDoc.UseCompatibleStateImageBehavior = false;
            this.m_ListViewDoc.View = System.Windows.Forms.View.Details;
            // 
            // m_ColumnHeaderNumber
            // 
            this.m_ColumnHeaderNumber.Text = "序号";
            this.m_ColumnHeaderNumber.Width = 40;
            // 
            // m_ColumnHeaderName
            // 
            this.m_ColumnHeaderName.Text = "名称";
            this.m_ColumnHeaderName.Width = 116;
            // 
            // m_ColumnHeaderSize
            // 
            this.m_ColumnHeaderSize.Text = "文件大小";
            this.m_ColumnHeaderSize.Width = 96;
            // 
            // m_ColumnHeaderDate
            // 
            this.m_ColumnHeaderDate.Text = "日期";
            this.m_ColumnHeaderDate.Width = 96;
            // 
            // m_ColumnHeaderFullPath
            // 
            this.m_ColumnHeaderFullPath.Text = "文件路径";
            this.m_ColumnHeaderFullPath.Width = 425;
            // 
            // m_TabPageImages
            // 
            this.m_TabPageImages.Controls.Add(this.m_ListViewImage);
            this.m_TabPageImages.Location = new System.Drawing.Point(0, 40);
            this.m_TabPageImages.Name = "m_TabPageImages";
            this.m_TabPageImages.Size = new System.Drawing.Size(801, 154);
            this.m_TabPageImages.TabIndex = 1;
            this.m_TabPageImages.Text = "照片";
            this.m_TabPageImages.UseVisualStyleBackColor = true;
            // 
            // m_ListViewImage
            // 
            this.m_ListViewImage.CheckBoxes = true;
            this.m_ListViewImage.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5});
            this.m_ListViewImage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_ListViewImage.FullRowSelect = true;
            this.m_ListViewImage.GridLines = true;
            this.m_ListViewImage.HideSelection = false;
            this.m_ListViewImage.Location = new System.Drawing.Point(0, 0);
            this.m_ListViewImage.Name = "m_ListViewImage";
            this.m_ListViewImage.Size = new System.Drawing.Size(801, 154);
            this.m_ListViewImage.TabIndex = 1;
            this.m_ListViewImage.UseCompatibleStateImageBehavior = false;
            this.m_ListViewImage.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "序号";
            this.columnHeader1.Width = 40;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "名称";
            this.columnHeader2.Width = 116;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "文件大小";
            this.columnHeader3.Width = 96;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "日期";
            this.columnHeader4.Width = 96;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "文件路径";
            this.columnHeader5.Width = 425;
            // 
            // m_TabPageVideos
            // 
            this.m_TabPageVideos.Controls.Add(this.m_ListViewVideo);
            this.m_TabPageVideos.Location = new System.Drawing.Point(0, 40);
            this.m_TabPageVideos.Name = "m_TabPageVideos";
            this.m_TabPageVideos.Size = new System.Drawing.Size(801, 154);
            this.m_TabPageVideos.TabIndex = 2;
            this.m_TabPageVideos.Text = "视频";
            this.m_TabPageVideos.UseVisualStyleBackColor = true;
            // 
            // m_ListViewVideo
            // 
            this.m_ListViewVideo.CheckBoxes = true;
            this.m_ListViewVideo.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader6,
            this.columnHeader7,
            this.columnHeader8,
            this.columnHeader9,
            this.columnHeader10});
            this.m_ListViewVideo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_ListViewVideo.FullRowSelect = true;
            this.m_ListViewVideo.GridLines = true;
            this.m_ListViewVideo.HideSelection = false;
            this.m_ListViewVideo.Location = new System.Drawing.Point(0, 0);
            this.m_ListViewVideo.Name = "m_ListViewVideo";
            this.m_ListViewVideo.Size = new System.Drawing.Size(801, 154);
            this.m_ListViewVideo.TabIndex = 1;
            this.m_ListViewVideo.UseCompatibleStateImageBehavior = false;
            this.m_ListViewVideo.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "序号";
            this.columnHeader6.Width = 40;
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "名称";
            this.columnHeader7.Width = 116;
            // 
            // columnHeader8
            // 
            this.columnHeader8.Text = "文件大小";
            this.columnHeader8.Width = 96;
            // 
            // columnHeader9
            // 
            this.columnHeader9.Text = "日期";
            this.columnHeader9.Width = 96;
            // 
            // columnHeader10
            // 
            this.columnHeader10.Text = "文件路径";
            this.columnHeader10.Width = 425;
            // 
            // m_FindRepeatBtn
            // 
            this.m_FindRepeatBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.m_FindRepeatBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.m_FindRepeatBtn.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(243)))), ((int)(((byte)(255)))));
            this.m_FindRepeatBtn.FillHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(160)))), ((int)(((byte)(255)))));
            this.m_FindRepeatBtn.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.m_FindRepeatBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(160)))), ((int)(((byte)(255)))));
            this.m_FindRepeatBtn.Location = new System.Drawing.Point(544, 54);
            this.m_FindRepeatBtn.MinimumSize = new System.Drawing.Size(1, 1);
            this.m_FindRepeatBtn.Name = "m_FindRepeatBtn";
            this.m_FindRepeatBtn.RectHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(160)))), ((int)(((byte)(255)))));
            this.m_FindRepeatBtn.Size = new System.Drawing.Size(114, 30);
            this.m_FindRepeatBtn.Style = Sunny.UI.UIStyle.LightBlue;
            this.m_FindRepeatBtn.Symbol = 57397;
            this.m_FindRepeatBtn.TabIndex = 5;
            this.m_FindRepeatBtn.Text = "检查重复";
            // 
            // m_DelBtn
            // 
            this.m_DelBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.m_DelBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.m_DelBtn.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(243)))), ((int)(((byte)(255)))));
            this.m_DelBtn.FillHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(160)))), ((int)(((byte)(255)))));
            this.m_DelBtn.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.m_DelBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(160)))), ((int)(((byte)(255)))));
            this.m_DelBtn.Location = new System.Drawing.Point(664, 54);
            this.m_DelBtn.MinimumSize = new System.Drawing.Size(1, 1);
            this.m_DelBtn.Name = "m_DelBtn";
            this.m_DelBtn.RectHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(160)))), ((int)(((byte)(255)))));
            this.m_DelBtn.Size = new System.Drawing.Size(117, 30);
            this.m_DelBtn.Style = Sunny.UI.UIStyle.LightBlue;
            this.m_DelBtn.Symbol = 57425;
            this.m_DelBtn.TabIndex = 6;
            this.m_DelBtn.Text = "删除选中";
            // 
            // m_GroupBox
            // 
            this.m_GroupBox.Controls.Add(this.m_UITextBoxVideos);
            this.m_GroupBox.Controls.Add(this.m_UITextBoxImages);
            this.m_GroupBox.Controls.Add(this.m_UITextBoxDocs);
            this.m_GroupBox.Controls.Add(this.label2);
            this.m_GroupBox.Controls.Add(this.m_UICheckBoxVideos);
            this.m_GroupBox.Controls.Add(this.m_UICheckBoxImages);
            this.m_GroupBox.Controls.Add(this.m_UICheckBoxDocs);
            this.m_GroupBox.Controls.Add(this.m_RadioButtonOld);
            this.m_GroupBox.Controls.Add(this.m_DelBtn);
            this.m_GroupBox.Controls.Add(this.label1);
            this.m_GroupBox.Controls.Add(this.m_FindRepeatBtn);
            this.m_GroupBox.Controls.Add(this.m_PathBrowerBtn);
            this.m_GroupBox.Controls.Add(this.m_WXFilesPathTextBox);
            this.m_GroupBox.Dock = System.Windows.Forms.DockStyle.Top;
            this.m_GroupBox.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.m_GroupBox.Location = new System.Drawing.Point(0, 35);
            this.m_GroupBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.m_GroupBox.MinimumSize = new System.Drawing.Size(1, 1);
            this.m_GroupBox.Name = "m_GroupBox";
            this.m_GroupBox.Padding = new System.Windows.Forms.Padding(0, 32, 0, 0);
            this.m_GroupBox.Size = new System.Drawing.Size(801, 194);
            this.m_GroupBox.Style = Sunny.UI.UIStyle.LightBlue;
            this.m_GroupBox.TabIndex = 7;
            this.m_GroupBox.TabStop = false;
            this.m_GroupBox.Text = "操作";
            // 
            // m_UITextBoxVideos
            // 
            this.m_UITextBoxVideos.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.m_UITextBoxVideos.FillColor = System.Drawing.Color.White;
            this.m_UITextBoxVideos.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.m_UITextBoxVideos.Location = new System.Drawing.Point(166, 150);
            this.m_UITextBoxVideos.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.m_UITextBoxVideos.Maximum = 2147483647D;
            this.m_UITextBoxVideos.Minimum = -2147483648D;
            this.m_UITextBoxVideos.MinimumSize = new System.Drawing.Size(1, 1);
            this.m_UITextBoxVideos.Name = "m_UITextBoxVideos";
            this.m_UITextBoxVideos.Padding = new System.Windows.Forms.Padding(5);
            this.m_UITextBoxVideos.Size = new System.Drawing.Size(615, 29);
            this.m_UITextBoxVideos.Style = Sunny.UI.UIStyle.LightBlue;
            this.m_UITextBoxVideos.TabIndex = 10;
            this.m_UITextBoxVideos.Text = ".mp4,.avi";
            // 
            // m_UITextBoxImages
            // 
            this.m_UITextBoxImages.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.m_UITextBoxImages.FillColor = System.Drawing.Color.White;
            this.m_UITextBoxImages.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.m_UITextBoxImages.Location = new System.Drawing.Point(166, 119);
            this.m_UITextBoxImages.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.m_UITextBoxImages.Maximum = 2147483647D;
            this.m_UITextBoxImages.Minimum = -2147483648D;
            this.m_UITextBoxImages.MinimumSize = new System.Drawing.Size(1, 1);
            this.m_UITextBoxImages.Name = "m_UITextBoxImages";
            this.m_UITextBoxImages.Padding = new System.Windows.Forms.Padding(5);
            this.m_UITextBoxImages.Size = new System.Drawing.Size(615, 29);
            this.m_UITextBoxImages.Style = Sunny.UI.UIStyle.LightBlue;
            this.m_UITextBoxImages.TabIndex = 10;
            this.m_UITextBoxImages.Text = ".png,.jpg,.jpeg,.gif,.bmp";
            // 
            // m_UITextBoxDocs
            // 
            this.m_UITextBoxDocs.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.m_UITextBoxDocs.FillColor = System.Drawing.Color.White;
            this.m_UITextBoxDocs.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.m_UITextBoxDocs.Location = new System.Drawing.Point(166, 88);
            this.m_UITextBoxDocs.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.m_UITextBoxDocs.Maximum = 2147483647D;
            this.m_UITextBoxDocs.Minimum = -2147483648D;
            this.m_UITextBoxDocs.MinimumSize = new System.Drawing.Size(1, 1);
            this.m_UITextBoxDocs.Name = "m_UITextBoxDocs";
            this.m_UITextBoxDocs.Padding = new System.Windows.Forms.Padding(5);
            this.m_UITextBoxDocs.Size = new System.Drawing.Size(615, 29);
            this.m_UITextBoxDocs.Style = Sunny.UI.UIStyle.LightBlue;
            this.m_UITextBoxDocs.TabIndex = 10;
            this.m_UITextBoxDocs.Text = ".doc,.docx,.xls,.xlsx,.ppt,.pptx,.pdf";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 59);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(148, 21);
            this.label2.TabIndex = 9;
            this.label2.Text = "文件完全相同时  ：";
            // 
            // m_UICheckBoxVideos
            // 
            this.m_UICheckBoxVideos.Checked = true;
            this.m_UICheckBoxVideos.Cursor = System.Windows.Forms.Cursors.Hand;
            this.m_UICheckBoxVideos.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.m_UICheckBoxVideos.Location = new System.Drawing.Point(9, 150);
            this.m_UICheckBoxVideos.MinimumSize = new System.Drawing.Size(1, 1);
            this.m_UICheckBoxVideos.Name = "m_UICheckBoxVideos";
            this.m_UICheckBoxVideos.Padding = new System.Windows.Forms.Padding(22, 0, 0, 0);
            this.m_UICheckBoxVideos.Size = new System.Drawing.Size(116, 25);
            this.m_UICheckBoxVideos.Style = Sunny.UI.UIStyle.LightBlue;
            this.m_UICheckBoxVideos.TabIndex = 7;
            this.m_UICheckBoxVideos.Text = "视频检查：";
            // 
            // m_UICheckBoxImages
            // 
            this.m_UICheckBoxImages.Checked = true;
            this.m_UICheckBoxImages.Cursor = System.Windows.Forms.Cursors.Hand;
            this.m_UICheckBoxImages.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.m_UICheckBoxImages.Location = new System.Drawing.Point(9, 119);
            this.m_UICheckBoxImages.MinimumSize = new System.Drawing.Size(1, 1);
            this.m_UICheckBoxImages.Name = "m_UICheckBoxImages";
            this.m_UICheckBoxImages.Padding = new System.Windows.Forms.Padding(22, 0, 0, 0);
            this.m_UICheckBoxImages.Size = new System.Drawing.Size(116, 25);
            this.m_UICheckBoxImages.Style = Sunny.UI.UIStyle.LightBlue;
            this.m_UICheckBoxImages.TabIndex = 7;
            this.m_UICheckBoxImages.Text = "照片检查：";
            // 
            // m_UICheckBoxDocs
            // 
            this.m_UICheckBoxDocs.Checked = true;
            this.m_UICheckBoxDocs.Cursor = System.Windows.Forms.Cursors.Hand;
            this.m_UICheckBoxDocs.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.m_UICheckBoxDocs.Location = new System.Drawing.Point(9, 88);
            this.m_UICheckBoxDocs.MinimumSize = new System.Drawing.Size(1, 1);
            this.m_UICheckBoxDocs.Name = "m_UICheckBoxDocs";
            this.m_UICheckBoxDocs.Padding = new System.Windows.Forms.Padding(22, 0, 0, 0);
            this.m_UICheckBoxDocs.Size = new System.Drawing.Size(116, 25);
            this.m_UICheckBoxDocs.Style = Sunny.UI.UIStyle.LightBlue;
            this.m_UICheckBoxDocs.TabIndex = 7;
            this.m_UICheckBoxDocs.Text = "文档检查：";
            // 
            // m_RadioButtonOld
            // 
            this.m_RadioButtonOld.Checked = true;
            this.m_RadioButtonOld.Cursor = System.Windows.Forms.Cursors.Hand;
            this.m_RadioButtonOld.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.m_RadioButtonOld.Location = new System.Drawing.Point(166, 57);
            this.m_RadioButtonOld.MinimumSize = new System.Drawing.Size(1, 1);
            this.m_RadioButtonOld.Name = "m_RadioButtonOld";
            this.m_RadioButtonOld.Padding = new System.Windows.Forms.Padding(22, 0, 0, 0);
            this.m_RadioButtonOld.Size = new System.Drawing.Size(139, 25);
            this.m_RadioButtonOld.Style = Sunny.UI.UIStyle.LightBlue;
            this.m_RadioButtonOld.TabIndex = 7;
            this.m_RadioButtonOld.Text = "自动选中旧文件";
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(801, 423);
            this.Controls.Add(this.m_TabControl);
            this.Controls.Add(this.m_GroupBox);
            this.ExtendBox = true;
            this.ExtendSymbol = 117;
            this.Name = "FrmMain";
            this.Style = Sunny.UI.UIStyle.LightBlue;
            this.Text = "WXClear-微信文件去重";
            this.m_TabControl.ResumeLayout(false);
            this.m_TabPageDocuments.ResumeLayout(false);
            this.m_TabPageDocuments.PerformLayout();
            this.m_StatusStrip.ResumeLayout(false);
            this.m_StatusStrip.PerformLayout();
            this.m_TabPageImages.ResumeLayout(false);
            this.m_TabPageVideos.ResumeLayout(false);
            this.m_GroupBox.ResumeLayout(false);
            this.m_GroupBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Sunny.UI.UILabel label1;
        private Sunny.UI.UITextBox m_WXFilesPathTextBox;
        private Sunny.UI.UISymbolButton m_PathBrowerBtn;
        private Sunny.UI.UITabControl m_TabControl;
        private System.Windows.Forms.TabPage m_TabPageDocuments;
        private System.Windows.Forms.TabPage m_TabPageImages;
        private System.Windows.Forms.TabPage m_TabPageVideos;
        private Sunny.UI.UISymbolButton m_FindRepeatBtn;
        private Sunny.UI.UISymbolButton m_DelBtn;
        private Sunny.UI.UIGroupBox m_GroupBox;
        private Sunny.UI.UICheckBox m_RadioButtonOld;
        private System.Windows.Forms.ListView m_ListViewDoc;
        private System.Windows.Forms.ColumnHeader m_ColumnHeaderNumber;
        private System.Windows.Forms.ColumnHeader m_ColumnHeaderName;
        private System.Windows.Forms.ColumnHeader m_ColumnHeaderSize;
        private System.Windows.Forms.ColumnHeader m_ColumnHeaderFullPath;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ColumnHeader m_ColumnHeaderDate;
        private System.Windows.Forms.ListView m_ListViewImage;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ListView m_ListViewVideo;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.ColumnHeader columnHeader8;
        private System.Windows.Forms.ColumnHeader columnHeader9;
        private System.Windows.Forms.ColumnHeader columnHeader10;
        private System.Windows.Forms.StatusStrip m_StatusStrip;
        private System.Windows.Forms.ToolStripStatusLabel m_LabelMessage;
        private Sunny.UI.UICheckBox m_UICheckBoxDocs;
        private Sunny.UI.UITextBox m_UITextBoxDocs;
        private Sunny.UI.UITextBox m_UITextBoxVideos;
        private Sunny.UI.UITextBox m_UITextBoxImages;
        private Sunny.UI.UICheckBox m_UICheckBoxVideos;
        private Sunny.UI.UICheckBox m_UICheckBoxImages;
    }
}

